import React , { Component } from 'react'

class Button extends Component{
    constructor(){
        super()        
    }
    render() {
        console.log('the component Button has updated')

        return(
        <button type='button'>{this.props.name}</button>
        )
    }
}

 export default Button

const Nav = function(props) {
    return (<div>Nav from
        {props.title}
        {props.children}


    </div>)
}
export {Nav}
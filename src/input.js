import React , {Component} from 'react'

//strach a tag, for e.g., input -> target
//controlled component
export default class Input extends Component{
    constructor(){
        super()
        this.state = {
            value : ''
        }
    }
    handleInput(e){
        // console.log(e.target.value) // sratch tag
        // console.log(e.nativeEvent) // strach original event
        // console.log(e)
        if(e.target.value.length > 10){
            return
        }
        this.setState({
            value: e.target.value
        })
    }
    render(){
        console.log('the component Input has updated')
        return(
            <input type="text" onInput={(e)=>this.handleInput(e)} value={this.state.value}/>
        )
    }
}
import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import plus , {minus} from './plus&minus'
import { render } from '@testing-library/react';
import Button, {Nav} from './Button'
import Input from './input'
class Animal{
  constructor(){
    this.type = 'animal'
  }
}
class Human extends Animal{
  constructor(height){
    super()
    this.height = height
  }
}
console.log(new Human(180))
// console.log(plus(1,2))
// console.log(minus(1,2))

//By class
// class Nav extends Component{
//   constructor(){
//     super()
//   }

//   render(){
//     return(
//       <div style={{color : 'white', backgroundColor : 'black'}}>NavBar</div>
//     )
//   }
// }

//By function
// const Button = function() {
//   return(
//     <button type="button">From the component: Button</button>
//   )
// }

class App extends React.Component {
  constructor(){
    super()
    // this.state = {
    //   like : false
    // }
    this.handleClick.bind(this)
  }
  handleClick(){
    // this.setState({
    //   like : !this.state.like
    // })
    console.log('you\'re calling handleclick method')
  }
  render() {
    console.log('component App updated')
    return (
      <div>
        {/* <Nav />
        <Button></Button>
      <button type="button" style={this.state.like ? {color : 'red'} : {color : 'black'}}
      onClick={()=>this.handleClick()}>
        {this.state.like ? "Liked" : "Like"}
        </button> */}

        {/* <p onClick={()=>this.handleClick()}>click to check this</p> */}

        {/* <Input></Input>
        <Button />
        <p onClick={()=>this.setState({})}>Click to update component App</p> */}
        
        <Nav title="title of nav">
          <h3>This is children1</h3>
        </Nav>
        <Button name="Click it"></Button>
        </div>
    )
  }
}
export default App;
